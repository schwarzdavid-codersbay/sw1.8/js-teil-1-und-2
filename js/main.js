const name = "David"

let x = 0

const inputElement = document.getElementById("myInput");
if(inputElement !== null) {
    x = inputElement.valueAsNumber
}

x += 5
x += 3
x /= 2

document.body.style.paddingTop = x + "px"

const isValid = false

parseInt("5.5") // 5
parseFloat("5.5") // 5.5

parseInt("Hallo") // NaN

const num1 = parseInt("hallo") // dynamischer wert aus einem eingabefeld
const num2 = parseInt("7") // dynamischer wert aus einem eingabefeld

console.log(num1 + num2)


const fiveSixResult = calculateSomething(5, 6)
const foo = calculateSomething(8, -2)


function calculateSomething(angle, x) {
    const y = Math.sin(angle) * x
    let z = 1

    if(y > 0.5) {
        console.log(name)
        z = 0
    }
    console.log(z) // undefined

    return y
}
